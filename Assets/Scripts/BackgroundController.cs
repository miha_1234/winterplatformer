﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WinterPlatformer
{
    public class BackgroundController : MonoBehaviour
    {
        [SerializeField]
        private Transform[] _backgrounds;
        [SerializeField]
        private float backgroundSpeed;
        private float[] startPosition;
        private float backgroundWidth = 0;
        private Vector2 direction = new Vector2();

        // Start is called before the first frame update
        void Start()
        {
            startPosition = new float[_backgrounds.Length];
            for (int i = 0; i < _backgrounds.Length; i++)
            {
                if (i == 0)
                {
                    startPosition[i] = _backgrounds[i].localPosition.x;
                }
                else
                {
                    if(backgroundWidth == 0)
                    backgroundWidth = Mathf.Abs(startPosition[i - 1] - _backgrounds[i].localPosition.x);
                    _backgrounds[i].localPosition = new Vector3(_backgrounds[i - 1].localPosition.x + backgroundWidth, _backgrounds[i - 1].localPosition.y, _backgrounds[i - 1].localPosition.z);
                    startPosition[i] = _backgrounds[i].localPosition.x;
                }
            }

        }

        public float NewBackgroundPosition(float positionX)
        {
            return positionX + (backgroundWidth * (_backgrounds.Length));
        }

        // Update is called once per frame
        void Update()
        {
            transform.Translate(new Vector3(-Time.deltaTime * backgroundSpeed, 0, 0));
        }
    }
}