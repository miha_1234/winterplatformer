﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

namespace WinterPlatformer
{

    public class GameManager : MonoBehaviour
    {
        public BackgroundController backgroundController;
        public static GameManager gm;
        [SerializeField] private PlatformSpawner _platformSpawner;
        public bool GenerateAirPlatforms;
        public bool GeneratePlatforms;

        public Vector2 platformSize;

        void Start()
        {
            if (gm == null) { 
                             gm = this;
                            }
            //_platformSpawner = GetComponent<PlatformSpawner>();
        }

        private void OnEnable()
        {
            OnSpawn();
        }

        public void ScoreIncrement(int amount)
        {
        }

        public void LivesManagement()
        {
        }

        public void OnDeath()
        {
        }

        public void OnSpawn()
        {
            if (GeneratePlatforms)
            {
                for (int x = 2; x <= platformSize.x; x++)
                {
                    for (int y = 1; y <= platformSize.y; y++)
                    {
                        if (!GenerateAirPlatforms && y == 1) y++;
                        GetComponent<GeneratePlatform>().PlatformGenerator(transform.position, new Vector2(x, y), GenerateAirPlatforms);
                    }
                }
                _platformSpawner.OrganiseObjects(_platformSpawner.PlatformAirPrefab, "PlatformAirPrefab");
                _platformSpawner.OrganiseObjects(_platformSpawner.PlatformGroundPrefab, "PlatformGroundPrefab");
            }

            _platformSpawner.InstantiatePlatforms(null, GenerateAirPlatforms);
            _platformSpawner.SpawnPlatforms();

        }
    }
}
