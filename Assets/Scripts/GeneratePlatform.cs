﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WinterPlatformer
{
    public class GeneratePlatform : MonoBehaviour
    {
        public float SingleRowTileColliderYSize = 0.73f;

        public Sprite[] SingleRowTiles;
        public Sprite[] TopTiles;
        public Sprite[] MiddleTiles;
        public Sprite[] BottomTiles;




        public void PlatformGenerator(Vector2 position, Vector2 size, bool airbourne)
        {
            //GameObject platformCloneParent = Instantiate(new GameObject("platformCloneParent"+size.x+"x"+size.y, typeof(BoxCollider2D)),position, Quaternion.identity, transform);
            GameObject platformCloneParent = new GameObject("platformCloneParent" + size.x + "x" + size.y, typeof(BoxCollider2D));
            platformCloneParent.transform.parent = transform;


            if (size.y == 1 && airbourne)
            {
                platformCloneParent.AddComponent<PlatformEffector2D>();
                for (int i = 0; i < size.x; i++)
                {
                    position.x = i;

                    GameObject clone = new GameObject("platformTileClone" + i, typeof(SpriteRenderer));
                    clone.transform.parent = platformCloneParent.transform;
                    clone.transform.position = position;
                    //clone = Instantiate(new GameObject("platformTileClone" + i, typeof(SpriteRenderer)), position, Quaternion.identity, platformCloneParent.transform);

                    if (i == 0)
                    {
                        clone.GetComponent<SpriteRenderer>().sprite = SingleRowTiles[i];
                    }

                    else if (i == size.x - 1)
                    {
                        clone.GetComponent<SpriteRenderer>().sprite = SingleRowTiles[SingleRowTiles.Length - 1];
                    }
                    else
                    {
                        clone.GetComponent<SpriteRenderer>().sprite = SingleRowTiles[Random.Range(1, SingleRowTiles.Length - 1)];
                    }
                }

                platformCloneParent.GetComponent<BoxCollider2D>().usedByEffector = true;
                platformCloneParent.GetComponent<BoxCollider2D>().size = new Vector2(size.x, SingleRowTileColliderYSize);
                platformCloneParent.GetComponent<BoxCollider2D>().offset = new Vector2(((size.x - 1) * 0.5f), 0);

                GetComponent<PlatformSpawner>().PlatformAirPrefab.Add(platformCloneParent);
            }

            else
            {
                for (int j = 0; j < size.y; j++)
                {

                    position.y = -j;

                    for (int i = 0; i < size.x; i++)
                    {
                        position.x = i;
                        GameObject clone = new GameObject("platformTileClone" + i, typeof(SpriteRenderer));
                        clone.transform.parent = platformCloneParent.transform;
                        clone.transform.position = position;

                        //clone = Instantiate(new GameObject("platformTileClone" + i, typeof(SpriteRenderer)), position, Quaternion.identity, platformCloneParent.transform);


                        if (j == size.y - 1)
                        {
                            if (i == 0)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = BottomTiles[i];
                            }

                            else if (i == size.x - 1)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = BottomTiles[BottomTiles.Length - 1];
                            }
                            else
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = BottomTiles[Random.Range(1, BottomTiles.Length - 1)];
                            }
                        }

                        else if (j == 0)
                        {
                            if (i == 0)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = TopTiles[i];
                            }

                            else if (i == size.x - 1)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = TopTiles[TopTiles.Length - 1];
                            }
                            else
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = TopTiles[Random.Range(1, TopTiles.Length - 1)];
                            }
                        }
                        else
                        {
                            if (i == 0)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = MiddleTiles[i];
                            }

                            else if (i == size.x - 1)
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = MiddleTiles[MiddleTiles.Length - 1];
                            }
                            else
                            {
                                clone.GetComponent<SpriteRenderer>().sprite = MiddleTiles[Random.Range(1, MiddleTiles.Length - 1)];
                            }

                        }

                        platformCloneParent.GetComponent<BoxCollider2D>().size = new Vector2(size.x, size.y);
                        platformCloneParent.GetComponent<BoxCollider2D>().offset = new Vector2(((size.x - 1) * 0.5f), -((size.y - 1) * 0.5f));

                    }
                }
                GetComponent<PlatformSpawner>().PlatformGroundPrefab.Add(platformCloneParent);
            }
            GetComponent<PlatformSpawner>().RemovePlatform(platformCloneParent);
        }
    }
}