﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WinterPlatformer
{
    public class PlatformSpawner : MonoBehaviour
    {
        public List<GameObject> PlatformGroundPrefab;
        public List<GameObject> PlatformAirPrefab;

        [SerializeField] private List<GameObject> _instantiatedPlatforms = new List<GameObject>();
        [SerializeField] private List<GameObject> _spawnedPlatforms = new List<GameObject>();

        [SerializeField] private Vector2 _spawnGroundOffsetMin;
        [SerializeField] private Vector2 _spawnGroundOffsetMax;
        [SerializeField] private Vector2 _spawnAirOffsetMin;
        [SerializeField] private Vector2 _spawnAirOffsetMax;

        [SerializeField] private GameObject _lastSpawnedPlatform;
        private Vector2 _lastSpawnPosition;
        private Vector2 _firstSpawnedPlatformPosition;

        private void Awake()
        {
            _lastSpawnPosition = _lastSpawnedPlatform.transform.position;
            _firstSpawnedPlatformPosition = _lastSpawnedPlatform.transform.position;
        }

        private void Start()
        {
            InstantiatePlatforms(PlatformGroundPrefab, false);
        }

        public void InstantiatePlatforms(List<GameObject> platforms, bool airbourne)
        {
            if (platforms == null)
            {
                platforms = PlatformGroundPrefab;
            }

            foreach (GameObject platform in platforms)
            {
                InstantiatePlatform(platform, NewPlatformPosition(_spawnGroundOffsetMin, _spawnGroundOffsetMax, platform));
            }

            if (airbourne)
            {
                platforms = PlatformAirPrefab;

                foreach (GameObject platform in platforms)
                {
                    InstantiatePlatform(platform, NewPlatformPosition(_spawnAirOffsetMin, _spawnAirOffsetMax, platform));
                }
            }
        }

        public Vector2 NewPlatformPosition(Vector2 offsetMin, Vector2 offsetMax, GameObject platform)
        {
            Vector2 position;
            float yPosition;

            if (platform.GetComponent<BoxCollider2D>().size.y > 1)
            {
                yPosition = -platform.GetComponent<BoxCollider2D>().size.y / 2;
            }
            else
            {
                yPosition = Random.Range(offsetMin.y, offsetMax.y) - platform.GetComponent<BoxCollider2D>().size.y / 2;
            }
            
            position = new Vector2(_lastSpawnPosition.x + _lastSpawnedPlatform.GetComponent<BoxCollider2D>().size.x / 2, 0) +
                                                  new Vector2(Random.Range(offsetMin.x, offsetMax.x) + platform.GetComponent<BoxCollider2D>().size.x / 2, yPosition);

            return position;
        }

        void InstantiatePlatform(GameObject prefab, Vector2 position)
        {
            GameObject clone;
            clone = Instantiate(prefab, position, Quaternion.identity, transform);
            clone.SetActive(false);
            _lastSpawnedPlatform = clone;
            //_lastSpawnPosition = clone.transform.position;
            _instantiatedPlatforms.Add(clone);
        }

        void SpawnPlatform(GameObject platform, Vector2 position)
        {
            platform.transform.position = position;
            _lastSpawnPosition = platform.transform.position;
            platform.SetActive(true);
            _spawnedPlatforms.Add(platform);
        }

        public void SpawnPlatforms()
        {
            _instantiatedPlatforms = ListRandomizer(_instantiatedPlatforms);
            _lastSpawnPosition = _firstSpawnedPlatformPosition;
            if (_instantiatedPlatforms != null)
            {
                foreach (GameObject platform in _instantiatedPlatforms)
                {
                    SpawnPlatform(platform, NewPlatformPosition(_spawnGroundOffsetMin, _spawnGroundOffsetMax, platform));
                }
            }
        }
        public void RemovePlatform(GameObject platform)
        {
            platform.SetActive(false);
            _spawnedPlatforms.Remove(platform);
        }

        private List<GameObject> ListRandomizer(List<GameObject> list)
        {
            for (int i = 0; i < list.Count; i++)
            {
                int randomInt = Random.Range(0, list.Count);
                GameObject temp = list[randomInt];
                list[randomInt] = list[i];
                list[i] = temp;
            }

            return list;
        }

        public void OrganiseObjects(List<GameObject> list, string name)
        {
            GameObject parent = new GameObject();
            parent.name = name + "Parent";
            foreach (GameObject platform in list)
            {
                platform.transform.parent = parent.transform;
            }
        }
    }
}