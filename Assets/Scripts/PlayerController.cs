﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace WinterPlatformer
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public class PlayerController : MonoBehaviour
    {
        private Transform _transform;
        private Collider2D _collider;
        private Rigidbody2D _rigidbody;
        private Animator _animator;
        private Camera _camera;
        private int _platformLayer;
        [SerializeField] string _platformLayerName = "Platform";

        [SerializeField] private bool _isWalking;
        private bool _isSliding;
        private bool _isJumping;
        private bool _isDead;

        [SerializeField] bool _isGrounded;
        [SerializeField] bool _canJump;
        [SerializeField] float _moveSpeed = 0.1f;
        [SerializeField] float _jumpPower;

        public bool IsWalking
        {
            get { return _isWalking; }
            set
            {
                if (value != _isWalking)
                { _isWalking = value; }
                _animator?.SetBool("IsWalking", _isWalking);
            }
        }

        public bool IsSliding {
            get { return _isSliding; }
            set {
                if (value != _isSliding) { _isSliding = value; }
                _animator?.SetBool("IsSliding", _isSliding);
            }
        }

        public bool IsJumping
        {
            get { return _isJumping; }
            set
            {
                if (value != _isJumping)
                { _isJumping = value; }
                _animator?.SetBool("IsJumping", _isJumping);
            }
        }

        // Start is called before the first frame update
        void Start()
        {
            _transform = transform;
            _collider = GetComponent<Collider2D>();
            _rigidbody = GetComponent<Rigidbody2D>();
            _animator = GetComponent<Animator>();
            _camera = Camera.main;

            _platformLayer = LayerMask.NameToLayer(_platformLayerName);
        }

        // Update is called once per frame
        void Update()
        {
            Move();
            _camera.GetComponent<Transform>().position = new Vector3(_transform.position.x, _camera.GetComponent<Transform>().position.y, _camera.GetComponent<Transform>().position.z);
        }

        private void FixedUpdate()
        {
            Jump();
            Slide();
        }

        private void Move()
        {
            if (Input.GetKey(KeyCode.D) || Input.GetKey(KeyCode.RightArrow))
            {
                if (_transform.localScale.x < 0)
                {
                    _transform.localScale = new Vector3(_transform.localScale.x * -1, _transform.localScale.y, _transform.localScale.z);
                }
                _transform.position += Vector3.right * _moveSpeed;
                IsWalking = true;
            }

            else if (Input.GetKey(KeyCode.A) || Input.GetKey(KeyCode.LeftArrow))
            {
                if (_transform.localScale.x > 0)
                {
                    _transform.localScale = new Vector3(_transform.localScale.x * -1, _transform.localScale.y, _transform.localScale.z);
                }
                _transform.position -= Vector3.right * _moveSpeed;
                IsWalking = true;
            }

            else
            {
                IsWalking = false;
                IsSliding = false;
            }
        }

        private void Jump()
        {
            if ((Input.GetKeyDown(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow)) && _canJump)
            {
                _rigidbody.velocity = Vector2.up * _jumpPower;
                _canJump = false;
                IsJumping = true;
            }
        }

        private void Slide()
        {
            if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S) && IsWalking)
            {

                IsSliding = true;
            }
            else
            {
                IsSliding = false;
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            IsJumping = false;
            _isGrounded = true;
            _canJump = true;
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            _isGrounded = false;
            _canJump = true;
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.layer == _platformLayer)
            {
                IsJumping = false;
                _isGrounded = true;
                _canJump = true;
            }
        }

        private void OnCollisionExit2D(Collision2D collision)
        {
            if (collision.gameObject.layer == _platformLayer)
            {
                _isGrounded = false;
                _canJump = true;
            }
        }

        private void OnBecameInvisible()
        {
            GameManager.gm.OnDeath();
        }
    }
}